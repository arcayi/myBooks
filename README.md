# myBooks

```bash
.
├── Books
│   ├── AI
│   │   ├── AI+in+Banking_+The+Reality+Behind+the+Hype+–+Financial+Times+–+Medium.pdf
│   │   ├── Deep learning in your browser_ A brisk guide – Towards Data Science.pdf
│   │   ├── Mastering+Blockchain+Second+Edition+9781788839044.pdf
│   │   ├── Present+your+data+science+results+in+a+Jupyter+notebook,+the+right+way.pdf
│   │   ├── Some+Essential+Hacks+and+Tricks+for+Machine+Learning+with+Python.pdf
│   │   └── 人工智能(AI)程序设计(面向对象语言).pdf
│   ├── C
│   │   ├── (AChapman & Hall book) Ray Toal, Rachel Rivera, Alexander Schneider, Eileen Choe-Programming Language Explorations-CRC Press_Chapman and Hall_CRC (2017).pdf
│   │   ├── An_Introduction_to_Econophysics_Correlations_and_C.pdf
│   │   ├── Brian J. Gough, Richard M. Stallman-An Introduction to GCC-Network Theory Ltd. (2004).pdf
│   │   ├── Brian Kernighan and Dennis Ritchie - The C Programming Language (1988, Prentice Hall PTR).pdf
│   │   ├── Brian Kernighan and Dennis Ritchie-The C Programming Language-Prentice Hall PTR (1988).pdf
│   │   ├── Brian W. Kernighan, Dennis M. Ritchie-The  C Programming Language, Second Edition (1988).pdf
│   │   ├── Brian W. Kernighan, Rob Pike - The Practice of Programming (1999, Addison-Wesley Professional).pdf
│   │   ├── Computer Systems: A Programmer’s Perspective.pdf
│   │   ├── C Traps and Pitfalls [Koenig 1989-01-11].pdf
│   │   ├── ctraps.pdf
│   │   ├── C和指针中文版.pdf
│   │   ├── C程序设计(C语言之父).pdf
│   │   ├── C语言的科学和艺术中文版.pdf
│   │   ├── Data-Structures-and-Algorithms-USING-C.pdf
│   │   ├── Easy Programming C Programming  Language  The ULtimate Beginners Guide.pdf
│   │   ├── Hagen-The Definitive Guide to GCC-Apress (2006).pdf
│   │   ├── Harry. H. Chaudhary.-C Programming __ The Ultimate Way to Learn The Fundamentals of The C Language.-CreateSpace Independent Publishing Platform (2014).pdf
│   │   ├── Linux C编程一站式学习.pdf
│   │   ├── Naveen Toppo, Hrishikesh Dewan (auth.) - Pointers in C_ A Hands on Approach (2013, Apress).pdf
│   │   ├── Noel Kalicharan-Learn to Program with C_ Learn to Program using the Popular C Programming Language-Apress (2015).pdf
│   │   ├── Peter Sestoft-Programming Language Concepts-Springer (2017).pdf
│   │   ├── Richard Reese - Understanding and Using C Pointers (2013, O'Reilly Media).pdf
│   │   ├── Richard Reese-Understanding and Using C Pointers-O'Reilly Media (2013).pdf
│   │   ├── Ruminations-on-C-A-Decade-of-Programming-Insight-and-Experience-C-.pdf
│   │   ├── Stephen G. Kochan Programming in C.epub
│   │   ├── [zixue7.com]_数据结构与算法分析：C语言描述（原书第2版）.pdf.crdownload
│   │   └── 代码阅读方法与实践.pdf
│   ├── C++
│   │   ├── Accelerated-C-Practical-Programming-by-Example.pdf
│   │   ├── (A Collection of Programming Interview Questions 3) Dr Antonio Gulli-A Collection of Bit Programming Interview Questions solved in C++-CreateSpace Independent Publishing Platform (2014).epub
│   │   ├── Adalat Khan-Learn Professional Programming Skill in C++ Programming Language-XLIBRIS (2013).azw3
│   │   ├── Andrei Alexandrescu - Modern C++ design_ generic programming and design patterns applied (2001, Addison-Wesley Professional).pdf
│   │   ├── Aristides Bouras, Loukia Ainarozidou-C++ and Algorithmic Thinking for the Complete Beginner_ Learn to Think Like a Programmer [Part 1 of 5] (2016).epub
│   │   ├── Assessing-and-Improving-Prediction-and-Classification-Theory-and-Algorithms-in-C-.pdf
│   │   ├── Bjarne Stroustrup-The C++ Programming Language, 4th Edition-Addison-Wesley Professional (2013).pdf
│   │   ├── C-How-to-Program-With-an-Introduction-to-C-.pdf
│   │   ├── Christopher Kormanyos (auth.)-Real-Time C++_ Efficient Object-Oriented and Template Microcontroller Programming-Springer-Verlag Berlin Heidelberg (2015).pdf
│   │   ├── -Data-Mining-Algorithms-in-C-Data-Patterns-and-Algorithms-for-Modern-Applications.pdf
│   │   ├── (Developer's library) Paul DuBois-MySQL-Addison-Wesley  (2009) .pdf
│   │   ├── Dontae Grose II-Programming in C++_ Volume I_ The Foundation of C++ (2017).epub
│   │   ├── Dorian P. Yeager-Object-Oriented Programming Languages and Event-Driven Programming-Mercury Learning & Information (2014).epub
│   │   ├── D. S. Malik-C++ Programming_ From Problem Analysis to Program Design-Course Technology (2014).pdf
│   │   ├── (For Dummies_ Computer_Tech) John Paul Mueller, Luca Massaron-Algorithms For Dummies-Wiley (2017).azw3
│   │   ├── Gaston C. Hillar-Learning Object-Oriented Programming-Packt Publishing (2015).pdf
│   │   ├── Hari Mohan Pandey-Object - Oriented Programming C++ Simplified-Laxmi Publications (2017).pdf
│   │   ├── Harper R-Practical Foundations for Programming Languages-Cambridge University Press (2016).pdf
│   │   ├── [Herb_Sutter,_Andrei_Alexandrescu]_C++_Coding_Stan(b-ok.org).epub
│   │   ├── Isaac D. Cody-Data Analytics and Linux Operating System-CreateSpace Independent Publishing Platform (2016).pdf
│   │   ├── Kurt Guntheroth - Optimized C++_ Proven Techniques for Heightened Performance (2016, O’Reilly Media).pdf
│   │   ├── (Lecture Notes in Computer Science 10101) Christian Bessiere, Luc De Raedt, Lars Kotthoff, Siegfried Nijssen, Barry O'Sullivan, Dino Pedreschi (eds.)-Data Mining and Constraint Programming_ Foundation.pdf
│   │   ├── Mark Lutz-Learning Python_ powerful object-oriented programming-O'Reilly Media (2013).epub
│   │   ├── Norman Matloff-The Art of Debugging With GDB and DDD (2013).pdf
│   │   ├── [Open Source Software Development Series] Jasmin Blanchette, Mark Summerfield - C++ GUI Programming with Qt 4 (2008, Prentice Hall in association with Trolltech Press).pdf
│   │   ├── Paul DuBois-MySQL Cookbook_ Solutions for Database Developers and Administrators-O'Reilly Media (2014).pdf
│   │   ├── progcpp.pdf
│   │   ├── Robert Harper-Practical Foundations for Programming Languages-Cambridge University Press (2012).pdf
│   │   ├── Robert Harper-Practical Foundations for Programming Languages-Cambridge University Press (2016).epub
│   │   ├── (Sams Teach Yourself) Siddhartha Rao-C++ in One Hour a Day-Sams Publishing (2017) (1).pdf
│   │   ├── (Sams Teach Yourself) Siddhartha Rao-C++ in One Hour a Day-Sams Publishing (2017).pdf
│   │   ├── Sengupta, Saumyendra_ Korobkin, Carl Phillip-C++ Object-oriented Data Structures-Korobkin, Carl P., Sengupta, Saumyendra., Springer-Verlag New York Inc (2014).pdf
│   │   ├── Steven F. Lott-Mastering Object-oriented Python-Packt Publishing (2014).pdf
│   │   ├── Stroustrup, Bjarne-Programming_ Principles and Practice Using C++ , Second Edition-Pearson Education_Addison-Wesley Professional (2015).epub
│   │   ├── Tatyana Sopronyuk, Nonna Shulga-150 C++ Programming Assignments. Variants of tasks & Examples of Code (2015).epub
│   │   ├── Tatyana Sopronyuk, Nonna Shulga-Object-oriented programming  in C++ (2014).epub
│   │   ├── T.R. Padmanabhan-Programming with Python-Springer (2017).pdf
│   │   ├── 我的第一本C++.pdf
│   │   ├── 面向对象程序设计—C++语言描述.pdf
│   │   └── 高质量程序设计指南C++C语言(第3版)+.pdf
│   ├── CS
│   │   ├── Andrew Koenig - C traps and pitfalls (1989, Addison-Wesley Professional).pdf
│   │   ├── Andy Hunt - Pragmatic Thinking and Learning_ Refactor Your Wetware (Pragmatic Programmers) (2008, Pragmatic Bookshelf).pdf
│   │   ├── [Andy_Oram,_Greg_Wilson]_Beautiful_Code_Leading_P(b-ok.xyz).pdf
│   │   ├── Ashwin Pajankar (auth.)-Python Unit Test Automation _ Practical Techniques for Python Developers and Testers-Apress (2017).pdf
│   │   ├── Bertrand Meyer (auth.) - Agile!_ The Good, the Hype and the Ugly (2014, Springer International Publishing).pdf
│   │   ├── [Book_CD-ROM] Bertrand Meyer - Object-Oriented Software Construction (Book_CD-ROM) (2000, Prentice Hall).pdf
│   │   ├── (Chapman & Hall_CRC the R series (CRC Press)) Matloff, Norman S.-Parallel computing for data science _ with examples in R, C++ and CUDA-CRC Press (2016).pdf
│   │   ├── Computer Age Statistical Inference.pdf
│   │   ├── Czarnul, Pawel - Parallel programming for modern high performance computing systems (2018, Chapman & Hall_CRC).pdf
│   │   ├── Data-Structures-and-Algorithm-Analysis-in-C-.pdf
│   │   ├── Girish Suryanarayana, Ganesh Samarthyam, Tushar Sharma-Refactoring for Software Design Smells_ Managing Technical Debt-Morgan Kaufmann (2014).pdf
│   │   ├── Hal Abelson_ Gerald Jay Sussman - Structure and Interpretation of Computer Programs (2002, MIT Press).mobi
│   │   ├── Hal Abelson_ Gerald Jay Sussman - Structure and Interpretation of Computer Programs (2002, MIT Press).pdf
│   │   ├── John F. Dooley (auth.)- Software Development, Design and Coding_ With Patterns, Debugging, Unit Testing, and Refactoring-Apress (2017).pdf
│   │   ├── Kent D. Lee - Foundations of Programming Languages (2018, Springer).pdf
│   │   ├── [Lecture Notes in Computer Science 10778] Roman Wyrzykowski, Jack Dongarra, Ewa Deelman, Konrad Karczewski - Parallel Processing and Applied Mathematics (2018, Springer International Publishing).pdf
│   │   ├── Manuel Mazzara, Bertrand Meyer - Present and Ulterior Software Engineering (2017, Springer).pdf
│   │   ├── [Mark_Allen_Weiss]_Algorithms,_data_structures,_an(b-ok.xyz).pdf
│   │   ├── [Mark_Allen_Weiss]_Algorithms,_Data_Structures,_an(b-ok.xyz).pdf
│   │   ├── [Mark_Allen_Weiss]_Data_Structures_and_Algorithm_A(b-ok.org).djvu
│   │   ├── Mark A. Weiss-Data Structures and Algorithm Analysis in C++-Pearson (2014).pdf.crdownload
│   │   ├── Maya Posch - Mastering C++ Multithreading_ Write robust, concurrent, and parallel applications (2017, Packt Publishing).pdf
│   │   ├── Roman Trobec, Boštjan Slivnik, Patricio Bulić, Borut Robič - Introduction to Parallel Computing. From Algorithms to Programming on State-of-the-Art Platforms (2018, Springer).pdf
│   │   ├── sicp.pdf
│   │   ├── [SpringerBriefs in Computational Intelligence] João Baúto,Rui Neves,Nuno Horta (auth.) -  Parallel Genetic Algorithms for Financial Pattern Discovery Using GPUs (2018, Springer International Publishing).pdf
│   │   ├── (Studies in computational intelligence 719) Lee, Roger-Computer and information science (2018).pdf.crdownload
│   │   ├── [The Morgan Kaufmann Series in Computer Architecture and Design] John L. Hennessy, David A. Patterson - Computer Architecture, Sixth Edition_ A Quantitative Approach (2017, Morgan Kaufmann).pdf
│   │   ├── (Theory in Practice) Andy Oram, Greg Wilson-Beautiful Code_ Leading Programmers Explain How They Think-O'Reilly Media (2007).pdf.crdownload
│   │   ├── 卓有成效的程序员(精选版).pdf
│   │   └── [数据结构与算法C..版].Data.Structures.and.Algorithms.in.C...(Adam.Drozdek).2nd.Ed.2001.pdf
│   ├── DataScience
│   │   ├── (Advances in Intelligent Systems and Computing 456) Maria Brigida Ferraro, Paolo Giordani, Barbara Vantaggi, Marek Gagolewski, María Ángeles Gil, Przemysław Grzegorzewski, Olgierd Hryniewicz (eds.)-So.pdf
│   │   ├── Alberto Cairo-The Truthful Art_ Data, Charts, and Maps for Communication-New Riders (2016).epub
│   │   ├── Allen B. Downey-Think Stats, 2nd Edition_ Exploratory Data Analysis-O'Reilly Media (2014).pdf
│   │   ├── Brian Steele, John Chandler, Swarna Reddy-Algorithms for Data Science-Springer (2017).pdf
│   │   ├── (Chapman and Hall_CRC the R Ser) Krider, Robert E._ Putler, Daniel S-Customer and Business Analytics _ Applied Data Mining for Business Decision Making Using R-CRC Press [Imprint, Taylor & Francis Gro.pdf
│   │   ├── Cole Nussbaumer Knaflic-Storytelling with Data_ A Data Visualization Guide for Business Professionals-Wiley (2015).pdf
│   │   ├── Command+Line+Tricks+For+Data+Scientists+–+Kade+Killary+–+Medium.pdf
│   │   ├── Douglas McIlwraith, Haralambos Marmanis, Dmitry Babenko-Algorithms of the Intelligent Web-Manning Publications (2016).pdf
│   │   ├── Jeroen Janssens Data Science at the Command Line Facing the Future with Time-Tested Tools.pdf
│   │   ├── Joel Grus-Data Science from Scratch_ First Principles with Python-O'Reilly Media (2015).pdf
│   │   ├── Modern Data Science with R.pdf
│   │   ├── Ralph Kimball, Margy Ross-The Data Warehouse Toolkit_ The Definitive Guide to Dimensional Modeling-Wiley (2013).pdf
│   │   ├── (Statistics in the social and behavioral sciences series) Ian Foster, Rayid Ghani, Ron S. Jarmin, Frauke Kreuter, Julia Lane-Big data and social science_ a practical guide to methods and tools-Chapman.pdf
│   │   ├── (Studies in Big Data 26) Srinivasan S. (ed.)-Guide to Big Data Applications-Springer (2018).pdf
│   │   ├── Ted Dunning, Ellen Friedman-Time Series Databases_ New Ways to Store and Access Data-O'Reilly Media (2014).pdf
│   │   ├── Torgo, Luís Data Mining with R Learning with Case Studies, Second Edition.pdf
│   │   ├── 常用SQL命令和VBA代码.doc
│   │   └── 数据分析修炼手册.docx
│   ├── Data Visualization
│   │   ├── Building Responsive Data Visualization for the Web.pdf
│   │   ├── Expert-Data-Visualization.pdf
│   │   ├── Interactive-Data-Visualization-Foundations-Techniques-and-Applications-Second-Edition.pdf
│   │   ├── Making-Sense-of-Data-Designing-Effective-Visualizations.pdf
│   │   └── Text-mining-and-visualization-case-studies-using-open-source-tools.pdf
│   ├── Econometrics
│   │   ├── Mastering-Metrics-The-Path-from-Cause-to-Effect.epub
│   │   ├── Mastering_’Metrics_The_Path_from_Cause_to_Effect.pdf
│   │   └── 高级计量经济学中文版_洪永淼.pdf
│   ├── Finance
│   │   ├── (A Chapman & Hall book) Masanobu Taniguchi, Hiroshi Shiraishi, Junichi Hirukawa, Hiroko Kato Solvang, Takashi Yamashita-Statistical Portfolio Estimation-CRC Press_Chapman and Hall_CRC (2018).pdf
│   │   ├── [Advanced Studies in Theoretical and Applied Econometrics 48] Jan Beran, Yuanhua Feng, Hartmut Hebbel (eds.) - Empirical Economic and Financial Research_ Theory, Methods and Practice (2015, Springer International Publishing).pdf
│   │   ├── Agarwal, Saurabh-Portfolio Selection Using Multi-Objective Optimisation-Springer International Publishing (2017).pdf
│   │   ├── Albina Unger (auth.)-The Use of Risk Budgets in Portfolio Optimization-Gabler Verlag (2015).pdf
│   │   ├── Balch, Tucker_ Romero, Phillip J-What Hedge Funds Do _ An Introduction to Quantitative Portfolio Management-Business Expert Press_McGraw-Hill Education (2014).pdf
│   │   ├── Ben Emons (auth.)-Mastering Stocks and Bonds_ Understanding How Asset Cross-Over Strategies will Improve Your Portfolio’s Performance-Palgrave Macmillan US (2015).pdf
│   │   ├── Bernhard Pfaff-Financial Risk Modelling and Portfolio Optimization with R-Wiley (2016).pdf
│   │   ├── Danielle DiMartino Booth-Fed Up_ An Insider’s Take on Why the Federal Reserve is Bad for America-Portfolio (2017).epub
│   │   ├── (Dynamic Modeling and Econometrics in Economics and Finance 18) Carl Chiarella, Willi Semmler, Chih-Ying Hsiao, Lebogang Mateane (auth.)-Sustainable Asset Accumulation and Dynamic Portfolio Decisions-.pdf
│   │   ├── (Financial Management Association survey and synthesis series) Kerry E. Back-Asset pricing and portfolio choice theory-Oxford University Press (2017).pdf
│   │   ├── (Frank J. Fabozzi Series) Frank J. Fabozzi, Dessislava A. Pachamanova-Portfolio Construction and Analytics-Wiley (2016).pdf
│   │   ├── H. Kent Baker, Greg Filbeck-Portfolio Theory and Management-Oxford University Press (2013).pdf
│   │   ├── Jacques Lussier-Successful Investing Is a Process_ Structuring Efficient Portfolios for Outperformance-Bloomberg Press (2013).pdf
│   │   ├── John B. Guerard, Jr. (eds.)-Portfolio Construction, Measurement, and Efficiency_ Essays in Honor of Jack Treynor-Springer International Publishing (2017).pdf
│   │   ├── (Management for Professionals) Marcus Schulmerich, Yves-Michel Leporcher, Ching-Hwa Eu (auth.)-Applied Asset and Risk Management_ A Guide to Modern Portfolio Management and Behavior-Driven Markets-Spr.pdf
│   │   ├── Matthew P. Erickson-Asset Rotation_ The Demise of Modern Portfolio Theory and the Birth of an Investment Renaissance-Wiley (2014).pdf
│   │   ├── Megha Agarwal (auth.)-Developments in Mean-Variance Efficient Portfolio Selection-Palgrave Macmillan UK (2015).pdf
│   │   ├── (New Developments in Quantitative Trading and Investment) Christian L. Dunis, Peter W. Middleton, Andreas Karathanasopolous, Konstantinos Theofilatos (eds.)-Artificial Intelligence in Financial Market.pdf
│   │   ├── Riccardo Rebonato, Alexander Denev-Portfolio Management under Stress_ A Bayesian-Net Approach to Coherent Asset Allocation-Cambridge University Press (2014).pdf
│   │   ├── Robert Dochow (auth.)-Online Algorithms for the Portfolio Selection Problem-Gabler Verlag (2016).pdf
│   │   ├── Robert Kissell (Auth.)-The Science of Algorithmic Trading and Portfolio Management-Academic Press (2013).pdf
│   │   ├── Russ Roberts-How Adam Smith Can Change Your Life_ An Unexpected Guide to Human Nature and Happiness-Portfolio Hardcover (2014).epub
│   │   ├── Stephen Satchell (eds.)-Asset Management_ Portfolio Construction, Performance and Returns-Palgrave Macmillan (2016).pdf
│   │   ├── (The Wiley Finance Series) Kula, Gökhan_ Raab, Martin-Beyond Smart Beta _ Index Investment Strategies for Modern Portfolio Management-John Wiley & Sons (2017).pdf
│   │   ├── Thomas K. Lloyd(auth.)-Successful Stock Signals for Traders and Portfolio Managers_ Integrating Technical Analysis with Fundamentals to Improve Performance-Wiley (2013).pdf
│   │   └── (Uncertainty and Operations Research) Zhongfeng Qin (auth.)-Uncertain Portfolio Optimization-Springer Singapore (2016).pdf
│   ├── GNU
│   │   ├── Bill McCarty-Learning Debian GNU_Linux-O'Reilly Media (1999).pdf
│   │   ├── Bill McCarty-Learning Debian GNU_Linux-O'Reilly Media (1999).pdf.crdownload
│   │   ├── Brian J. Gough, Richard M. Stallman-An Introduction to GCC_ For the GNU Compilers GCC and G++-Network Theory Ltd. (2004).pdf
│   │   ├── (Expert's Voice in Linux) Chris Johnson-Pro Bash Programming_ Scripting the GNU_Linux Shell-Apress (2009).pdf
│   │   ├── (Nutshell Handbooks) Robert Mecklenburg-Managing Projects with GNU Make -O'Reilly Media (2004).pdf
│   │   ├── (Programming Series) M. Tim(M. Tim Jones) Jones-GNU_Linux application programming-Charles River Media (2008).pdf
│   │   ├── Richard M. Stallman, Roland Pesch, Stan Shebs-Debugging with GDB_ The GNU Source-Level Debugger-Free Software Foundation (2002).pdf
│   │   ├── Steve Hunger-Debian GNU_Linux Bible-Hungry Minds (2001).pdf
│   │   └── Steve Hunger-Debian GNU_Linux Bible-Hungry Minds (2001).pdf.crdownload
│   ├── HFT
│   │   ├── High_Frequency_Trading_and_Modeling_in_Finance.pdf
│   │   ├── High-frequency trading - a practical guide to algorithmic strategies and trading systems.pdf
│   │   ├── High-frequency Trading.pdf
│   │   └── [Zhaodong_Wang,_Weian_Zheng]_High-Frequency_Tradining_and_Probability_Theory.pdf
│   ├── Investing
│   │   ├── Frank K. Martin, John C. Bogle-A Decade of Delusions_ From Speculative Contagion to the Great Recession-Wiley (2011).pdf
│   │   ├── John C. Bogle, Arthur Levitt Jr-The Clash of the Cultures_ Investment vs. Speculation-Wiley (2012).epub
│   │   └── (Little Books. Big Profits) John C. Bogle-The Little Book of Common Sense Investing_ The Only Way to Guarantee Your Fair Share of Stock Market Returns-Wiley (2017).epub
│   ├── Linux
│   │   ├── Arnold Robbins-Bash Pocket Reference_ Help for Power Users and Sys Admins-O'Reilly Media (2016).pdf
│   │   ├── Carl Albing, JP Vossen-bash Cookbook_ Solutions and Examples for bash Users-O’Reilly Media (2017).pdf
│   │   ├── Chris Johnson, Jayant Varma-Pro Bash Programming, Second Edition_ Scripting the GNU_Linux Shell-Apress (2015) (1).pdf
│   │   ├── Chris Johnson, Jayant Varma-Pro Bash Programming, Second Edition_ Scripting the GNU_Linux Shell-Apress (2015).pdf
│   │   ├── Command-Line-Kung-Fu-Bash-Scripting-Tricks-Linux-Shell-Programming-Tips-and-Bash-One-liners.pdf
│   │   ├── Dan Peleg-Mastering Sublime Text-Packt Publishing (2013).pdf
│   │   ├── (developer’s series) William “Bo” Rothwell-Linux for Developers Jumpstart Your Linux Programming Skills-Addison-Wesley (2017).pdf
│   │   ├── Emma Jane Hogbin Westby-Git for Teams_ A User-Centered Approach to Creating Efficient Workflows in Git-O'Reilly Media (2015).pdf
│   │   ├── Ferdinando Santacroce - Git Essentials (2017, Packt Publishing).pdf
│   │   ├── Giorgio Zarrelli-Mastering Bash-Packt Publishing (2017).azw3
│   │   ├── Jakub Narebski-Mastering Git_ Attain expert-level proficiency with Git for enhanced productivity and efficient collaboration by mastering advanced distributed version control features-Packt Publishing.pdf
│   │   ├── Jonathan Bates-Linux Mastery  The Ultimate Linux Operating System and Command Line Mastery-CreatеSpace Indepеndent Publishing Platform (2016).pdf
│   │   ├── Laster, Brent-Professional Git-Wrox (2016).pdf
│   │   ├── Linux-Administration-A-Beginner-s-Guide.pdf
│   │   ├── -Linux command line cookbook.pdf
│   │   ├── Machtelt Garrels-BASH Guide for Beginners-CreateSpace (2009).pdf
│   │   ├── Mark G. Sobell, Matthew Helmke-A practical Guide to Linux Commands, Editors and Shell Programming-Addison-Wesley (2018).pdf
│   │   ├── Matt Bailey-CoreOS in Action.  Running Applications on Container Linux-Manning Publications (2017).pdf
│   │   ├── Mendel Cooper-Advanced Bash-Scripting Guide (2008).pdf
│   │   ├── Mendel Cooper-Advanced Bash-Scripting Guide_ An in-depth exploration of the art of shell scripting (2014).pdf
│   │   ├── Mike McQuaid - Git in Practice (2014, Manning Publications).pdf
│   │   ├── Nicholas Marsh-Introduction to the Command Line (Second Edition)_ The Fat Free Guide to Unix and Linux Commands  .pdf
│   │   ├── Petru Isfan, Bogdan Vaida-Working with Linux – Quick Hacks for the Command Line-Packt.pdf
│   │   ├── Pro-Bash-Programming-2nd-Edition-Scripting-the-GNU-Linux-Shell.pdf
│   │   ├── Richard Blum, Christine Bresnahan-Linux Command Line and Shell Scripting Bible-Wiley (2015).pdf
│   │   ├── Sander van Vugt-Beginning the Linux Command Line-Apress (2015).pdf
│   │   ├── Shaumik Daityari - Jump Start Git (2015, SitePoint).pdf
│   │   ├── Steve Parker-Shell Scripting_ Expert Recipes for Linux, Bash and More  -John Wiley & Sons (2011).pdf
│   │   ├── UNIX-and-Linux-System-Administration-Handbook.pdf
│   │   ├── William E. Shotts, Jr.-The Linux command line_ a complete introduction-No Starch Press (2013).pdf
│   │   ├── 只是为了好玩-Linus Torvalds  自传.pdf
│   │   └── 珍藏--只是为了好玩：Linux之父林纳斯自传.pdf
│   ├── MachineLearning
│   │   ├── (Adaptive Computation and Machine Learning series) Ethem Alpaydin-Introduction to Machine Learning-The MIT Press (2014).pdf
│   │   ├── Akansu, Ali N._ Kulkarni, Sanjeev_ Malioutov, Dmitry-Financial signal processing and machine learning-Wiley _ IEEE Press (2016).pdf
│   │   ├── Atul Tripathi-Practical machine learning cookbook-Packt Publishing (2017).pdf
│   │   ├── Aurélien Géron-Hands-On Machine Learning with Scikit-Learn and TensorFlow_ Concepts, Tools, and Techniques to Build Intelligent Systems-O’Reilly Media (2017).epub
│   │   ├── Bashier, Eihab Bashier Mohammed_ Khan, Muhammad Badruddin_ Mohammed, Mohssen-Machine learning_ algorithms and applications-CRC Press (2017).epub
│   │   ├── Bruce Ratner-Statistical and Machine-Learning Data Mining_ Techniques for Better Predictive Modeling and Analysis of Big Data, Second Edition-CRC Press (2011).pdf
│   │   ├── Danish Haroon-Python Machine Learning Case Studies_ Five Case Studies for the Data Scientist-Apress (2017).pdf.crdownload
│   │   ├── David Barber-Bayesian Reasoning and Machine Learning-Cambridge University Press (2014).pdf
│   │   ├── Davy Cielen, Arno Meysman, Mohamed Ali-Introducing Data Science_ Big Data, Machine Learning and More, Using Python tools-Manning Publications (2016).pdf
│   │   ├── Davy Cielen, Arno Meysman, Mohamed Ali-Introducing Data Science_ Big Data, Machine Learning and More, Using Python tools-Manning Publications (2016).pdf.crdownload
│   │   ├── Ethem Alpaydin-Machine Learning.  The New AI-The MIT Press (2017).pdf
│   │   ├── Fundamentals of Machine Learning for Predictive Data Analytics.pdf
│   │   ├── (Information Science and Statistics) Vladimir Vapnik-The Nature Of Statistical Learning-Springer (2010).pdf
│   │   ├── (Intelligent Systems Reference Library 109) Achim Zielesny-From Curve Fitting to Machine Learning_ An Illustrative Guide to Scientific Data Analysis and Computational Intelligence-Springer (2016).pdf (1).crdownload
│   │   ├── (Intelligent Systems Reference Library 109) Achim Zielesny-From Curve Fitting to Machine Learning_ An Illustrative Guide to Scientific Data Analysis and Computational Intelligence-Springer (2016).pdf.crdownload
│   │   ├── Karthik Ramasubramanian, Abhishek Singh (auth.)-Machine Learning Using R-Apress (2017).pdf
│   │   ├── [Kevin_P._Murphy]_Machine_Learning_A_Probabilisti(b-ok.org).pdf
│   │   ├── Kubát, Miroslav-An Introduction to Machine Learning-Springer International Publishing _ Imprint_ Springer (2017).pdf
│   │   ├── Luis Pedro Coelho, Willi Richert-Building Machine Learning Systems with Python, 2nd Edition_ Get more from your data through creating practical machine learning systems with Python-Packt Publishing (2.pdf
│   │   ├── Mark Stamp-Introduction to Machine Learning with Applications in Information Security-CRC (2018).pdf
│   │   ├── Masashi Sugiyama-Introduction to Statistical Machine Learning-Morgan Kaufmann Publishers (2016) (1).pdf
│   │   ├── Masashi Sugiyama-Introduction to Statistical Machine Learning-Morgan Kaufmann Publishers (2016).pdf
│   │   ├── Masashi Sugiyama-Statistical Reinforcement Learning_ Modern Machine Learning Approaches-Chapman and Hall_CRC (2015).pdf
│   │   ├── Matthew Kirk-Thoughtful Machine Learning with Python  A Test-Driven Approach-O'Reilly (2017).pdf
│   │   ├── Matthew Kirk-Thoughtful Machine Learning with Python_ A Test-Driven Approach-O’Reilly (2017).pdf
│   │   ├── Mohssen Mohammed, Muhammad Badruddin Khan, Eihab Bashier Mohammed Bashier-Machine Learning. Algorithms and Applications-CRC (2017).pdf
│   │   ├── (Morgan Kaufmann Series in Data Management Systems) Ian H. Witten, Eibe Frank, Mark A. Hall, Christopher J. Pal-Data Mining_ Practical Machine Learning Tools and Techniques-Morgan Kaufmann (2016).pdf
│   │   ├── (Multivariate Analysis 1) Alboukadel Kassambara-Practical Guide to Cluster Analysis in R. Unsupervised Machine Learning-STHDA (2017).pdf
│   │   ├── N D Lewis-Deep Time Series Forecasting with Python_ An Intuitive Introduction to Deep Learning for Applied Time Series Modeling-CreateSpace Independent Publishing Platform (2016).pdf
│   │   ├── N D Lewis-Machine Learning Made Easy with R.  An Intuitive Step by Step Blueprint for Beginners-CreateSpace Independent (2017).pdf
│   │   ├── N.D. Lewis-Neural Networks for Time Series Forecasting with R-N.D. Lewis (2017).pdf
│   │   ├── (Net Developers) Sergios Theodoridis-Machine Learning_ A Bayesian and Optimization Perspective-Academic Press (2015).pdf.crdownload
│   │   ├── Nikhil Buduma, Nicholas Locascio-Fundamentals of Deep Learning_ Designing Next-Generation Machine Intelligence Algorithms-O’Reilly Media (2017).pdf
│   │   ├── Nikhil Ketkar-Deep Learning with Python. A Hands-on Introduction-Apress (2017).pdf
│   │   ├── Raghav Bali, Dipanjan Sarkar-R Machine Learning by Example-Packt (2016).pdf
│   │   ├── Reinforcement-Learning-An-Introduction.pdf
│   │   ├── (Springer Texts in Statistics) Alan J. Izenman (auth.)-Modern multivariate statistical techniques_ Regression, classification, and manifold learning-Springer-Verlag New York (2008).pdf
│   │   ├── (Springer Texts in Statistics) Gareth James, Daniela Witten, Trevor Hastie, Robert Tibshirani-An Introduction to Statistical Learning with Applications in R-Springer (2014).pdf
│   │   ├── (Springer Texts in Statistics) Richard A. Berk (auth.)-Statistical Learning from a Regression Perspective-Springer International Publishing (2016).pdf
│   │   ├── (Studies in Classification, Data Analysis, and Knowledge Organization) Udo Bankhofer, Dieter William Joenssen (auth.), Myra Spiliopoulou, Lars Schmidt-Thieme, Ruth Janning (eds.)-Data Analysis, Machin.pdf
│   │   ├── Sugiyama, Masashi-Introduction to Statistical Machine Learning-Morgan Kaufmann (2016).pdf
│   │   └── Viswa Viswanathan et al.-R_ Recipes for Analysis, Visualization and Machine Learning-Packt Publishing (2016).pdf
│   ├── Python
│   │   ├── Acodemy-Python_ Learn Web Scraping with Python In A DAY! _ The Ultimate Crash Course to Learning the Basics of Web Scraping with Python In No Time (2015).epub
│   │   ├── Aditya Bhargava-Grokking Algorithms_ An Illustrated Guide for Programmers and Other Curious People-Manning Publications (2016).pdf
│   │   ├── Ajay Ohri-Python for R Users-Wiley (2017).pdf
│   │   ├── Alex Martelli, Anna Ravenscroft, Steve Holden-Python in a Nutshell. A Desktop Quick Reference-O’Reilly (2017).pdf
│   │   ├── Ashwin Pajankar (auth.)-Python Unit Test Automation _ Practical Techniques for Python Developers and Testers-Apress (2017).pdf
│   │   ├── Automate-it-Recipes-to-upskill-your-business.pdf
│   │   ├── Automate the Boring Stuff with Python.pdf
│   │   ├── B. M. Harwani - Introduction to Python Programming and Developing GUI Applications with PyQT (2011, Cengage Learning PTR).pdf
│   │   ├── B.M. Harwani - Qt5 Python GUI Programming Cookbook (2018, Packt).pdf
│   │   ├── Brett Slatkin-Effective Python_ 59 SPECIFIC WAYS TO WRITE BETTER PYTHON-Addison-Wesley (2015).pdf
│   │   ├── Brian Okken - Python Testing with pytest_ Simple, Rapid, Effective, and Scalable (2017, Pragmatic Bookshelf).pdf
│   │   ├── Cathy O'Neil, Rachel Schutt-Doing Data Science_ Straight Talk from the Frontline-O'Reilly Media (2013).pdf
│   │   ├── Clinton W. Brownley-Foundations for Analytics with Python-O’Reilly Media (2016).epub
│   │   ├── (Community experience distilled) Sams, Prashanth-Selenium Essentials _ get to grips with automated web testing with the amazing power of Selenium WebDriver-Packt Publishing - ebooks Account (2015).pdf
│   │   ├── Comparative-Approaches-to-Using-R-and-Python-for-Statistical-Data-Analysis.pdf
│   │   ├── Cyrille Rossant-IPython Interactive Computing and Visualization Cookbook_ Over 100 hands-on recipes to sharpen your skills in high-performance numerical computing and data science with Python-Packt Pu.pdf
│   │   ├── Daniel Y. Chen-Pandas for Everyone.  Python Data Analysis-Addison-Wesley Professional (2017).pdf
│   │   ├── Data Science Essentials in Python Collect - Organize - Explore - Predict - Value.pdf
│   │   ├── Data-Structures-and-Algorithms-with-Python.pdf
│   │   ├── David Paper - Data Science Fundamentals for Python and MongoDB (2018, Apress).pdf
│   │   ├── Davy Cielen, Arno Meysman, Mohamed Ali-Introducing Data Science_ Big Data, Machine Learning, and more, using Python tools-Manning Publications (2016).pdf
│   │   ├── Doglio F.-Mastering Python High Performance.mobi
│   │   ├── Earnest Wish, Leo-Python Web Hacking Essentials-Amazon Digital Services, Inc. (2015).pdf
│   │   ├── Ethem Alpaydin-Machine Learning.  The New AI-The MIT Press (2017).pdf
│   │   ├── Explorations-in-Computing-An-Introduction-to-Computer-Science-and-Python-Programming.pdf
│   │   ├── Fluent Python - Clear, Concise, and Effective Programming.pdf
│   │   ├── Gábor László Hajba - Website Scraping with Python_ Using Beautifulsoup and Scrapy (2018, Apress).pdf
│   │   ├── [Giancarlo_Zaccone]_Python_Parallel_Programming_Co(b-ok.org).pdf
│   │   ├── Herron Philip. - Learning Cython Programming (Code Only) .zip
│   │   ├── HighPerformancePythonfromTrainingatEuroPython2011_v0.2.pdf
│   │   ├── High Performance Python.pdf
│   │   ├── Intermediate-Python.pdf
│   │   ├── IPython-Interactive-Computing-and-Visualization-Cookbook-Over-100-hands-on-recipes-to-sharpen-your-skills-in-high-performance-numerical-computing-and-data-science-with-Python.pdf
│   │   ├── Jacob Zimmerman-Python Descriptors-Apress (2017).pdf
│   │   ├── Jacqueline Kazil, Katharine Jarmul-Data Wrangling with Python_ Tips and Tools to Make Your Life Easier-O'Reilly Media (2016).pdf
│   │   ├── Jake VanderPlas-Python Data Science Handbook_ Essential Tools for Working with Data-O’Reilly Media (2016).epub
│   │   ├── Jake VanderPlas-Python Data Science Handbook_  Essential Tools for Working with Data-O’Reilly Media (2016).pdf
│   │   ├── John M. Stewart-Python for Scientists-Cambridge University Press (2017).pdf
│   │   ├── John V. Guttag-Introduction to Computation and Programming Using Python_ With Application to Understanding Data-The MIT Press (2016).mobi
│   │   ├── J. R. Parker-Python  An Introduction to Programming-Mercury Learning & Information (2016).pdf
│   │   ├── Kenneth Reitz, Tanya Schlusser-The Hitchhiker’s Guide to Python_ Best Practices for Development-O’Reilly Media (2016).pdf
│   │   ├── Kurt W. Smith - Cython_ A Guide for Python Programmers (2015, O’Reilly Media).pdf
│   │   ├── Kyran Dale-Data Visualization with Python and JavaScript_ Scrape, Clean, Explore & Transform Your Data-O’Reilly Media (2016).pdf
│   │   ├── Learning-Cython-Programming.pdf
│   │   ├── Luciano Ramalho-Fluent Python-O'Reilly Media (2015).pdf
│   │   ├── Magnus Lie Hetland-Beginning Python_ From Novice to Professional-Apress (2017).pdf
│   │   ├── Mastering-Predictive-Analytics-with-Python.pdf
│   │   ├── Michael Heydt-Learning pandas_ Get to grips with pandas - a versatile and high-performance Python library for data manipulation, analysis, and discovery-Packt Publishing (2015).pdf
│   │   ├── Michael Heydt-Mastering Pandas for Finance-Packt Publishing (2015).pdf
│   │   ├── Michael Heydt - Python Web Scraping Cookbook_ Over 90 proven recipes to get you scraping with Python, micro services, Docker and AWS (2018, Packt Publishing).pdf
│   │   ├── Michael Stueben - Good Habits for Great Coding_ Improving Programming Skills with Examples in Python (2018, Apress).pdf
│   │   ├── [Micha_Gorelick,_Ian_Ozsvald]_High_Performance_Pyt(b-ok.org).pdf
│   │   ├── Micha Gorelick, Ian Ozsvald-High Performance Python-O’Reilly (2015) (1).epub
│   │   ├── Micha Gorelick, Ian Ozsvald-High Performance Python-O’Reilly (2015).epub
│   │   ├── Micha Gorelick, Ian Ozsvald-High Performance Python_ Practical Performant Programming for Humans-O'Reilly Media (2014).pdf
│   │   ├── Michal Jaworski, Tarek Ziade-Expert Python Programming, 2nd Edition_ Become an ace Python programmer by learning best coding practices and advance-level concepts with Python 3.5-Packt Publishing (2016.pdf
│   │   ├── Michal Jaworski, Tarek Ziade-Expert Python Programming-Packt Publishing (2016).epub
│   │   ├── Narasimha Karumanchi-Data Structure and Algorithmic Thinking with Python  Data Structure and Algorithmic Puzzles-CareerMonk Publications (2016).pdf
│   │   ├── Noah Gift, Jeremy Jones-Python for Unix and Linux System Administration-O'Reilly Media (2008).pdf
│   │   ├── Percival, H.J.W.-Test-Driven Development with Python_ Obey the Testing Goat_ Using Django, Selenium, and JavaScript-O’Reilly Media (2017).pdf
│   │   ├── Peter R. Turner, Thomas Arildsen, Kathleen Kavanagh - Applied Scientific Computing with Python (2018, Springer).pdf
│   │   ├── Prabhanjan Tattar, Tony Ojeda, Sean Patrick Murphy, Benjamin Bengfort, Abhijit Dasgupta-Practical Data Science Cookbook_ Data pre-processing, analysis and visualization using R and Python-Packt Publis (1).pdf
│   │   ├── Prabhanjan Tattar, Tony Ojeda, Sean Patrick Murphy, Benjamin Bengfort, Abhijit Dasgupta-Practical Data Science Cookbook_ Data pre-processing, analysis and visualization using R and Python-Packt Publis.pdf
│   │   ├── [Prentice Hall open source software development series] Mark Summerfield - Rapid GUI programming with Python and Qt_ the definitive guide to PyQt programming (2008, Prentice Hall).pdf
│   │   ├── Python-201-Intermediate-Python.pdf
│   │   ├── Python-Data-Structures-and-Algorithms.pdf
│   │   ├── Python_High_Performance，2ed.pdf
│   │   ├── Python-High-Performance.pdf
│   │   ├── Python High Performance Programming - Lanaro, Gabriele.pdf
│   │   ├── Python-master-the-art-of-design-patterns.pdf
│   │   ├── Python Programming Advanced.djvu
│   │   ├── Python-Programming-An-Introduction-to-Computer-Science.pdf
│   │   ├── Python-Programming-Python-Programming-for-Beginners-Python-Programming-for-Intermediates.pdf
│   │   ├── Python-Unlocked-Become-more-fluent-in-Python-learn-strategies-and-techniques-for-smart-and-high-performance-Python-programming.pdf
│   │   ├── python自动化运维.pdf
│   │   ├── Rakesh Vidya Chandra, Bala Subrahmanyam Varanasi-Python Requests Essentials-Packt Publishing - ebooks Account (2015).pdf
│   │   ├── Richard Lawson-Web Scraping with Python-Packt Publishing (2015).pdf
│   │   ├── Rob Miles - Begin to code with Python (2018, Microsoft).pdf
│   │   ├── Ryan Mitchell-Web Scraping with Python_ Collecting Data from the Modern Web-O'Reilly Media (2015).pdf
│   │   ├── Ryan Mitchell - Web Scraping with Python_ Collecting More Data from the Modern Web (2018, O’Reilly Media).pdf
│   │   ├── Satya Avasarala-Selenium WebDriver Practical Guide-Packt Publishing (2014).pdf
│   │   ├── Steven F. Lott-Mastering Object-oriented Python-Packt Publishing (2014).pdf
│   │   ├── Svein Linge, Hans Petter Langtangen-Programming for Computations – Python. A Gentle Introduction to Numerical Simulations with Python-Springer (2016).pdf
│   │   ├── The Code Academy-Python Programming_ An In-Depth Guide Into The Essentials Of Python Programming-CreateSpace Independent Publishing Platform (2017).pdf
│   │   ├── The-Hacker-s-Guide-to-Python.pdf
│   │   ├── (The Pragmatic Programmers) Dmitry Zinoviev-Data Science Essentials in Python_ Collect - Organize - Explore - Predict - Value-Pragmatic Bookshelf (2016).pdf
│   │   ├── T.R. Padmanabhan-Programming with Python-Springer (2017).pdf
│   │   ├── (Undergraduate Topics in Computer Science) Laura Igual, Santi Segu-Introduction to Data Science.  A Python Approach to Concepts, Techniques and Applications-Springer (2017).pdf
│   │   ├── Unmesh Gundecha-Learning Selenium Testing Tools with Python_ A practical guide on automated web testing with Selenium using Python-Packt Publishing (2014).pdf
│   │   ├── Unmesh Gundecha Learning Selenium Testing Tools with Python A practical guide on automated web testing with Selenium using Python.pdf
│   │   ├── Unmesh Gundecha-Learning Selenium Testing Tools with Python-Packt Publishing (2014).pdf
│   │   ├── Vineeth G. Nair-Getting Started with Beautiful Soup_ Build your own web scraper and learn all about web scraping with Beautiful Soup-Packt Publishing (2014).pdf
│   │   ├── Violent Python.pdf
│   │   ├── Web Scraping with Python Collecting Data from the Modern Web.pdf
│   │   ├── Web+Scraping+with+Python+Collecting+More+Data+from+the+Modern+Web+2nd+Edition+9781491985571.pdf
│   │   └── Wessel Badenhorst-Practical Python Design Patterns. Pythonic Solutions to Common Problems-Apress (2017).pdf
│   ├── Quant
│   │   ├── (4-Volume Set) Rama Cont-Encyclopedia of Quantitative Finance-Wiley (2010).pdf
│   │   ├── Adam Ozanne (auth.)-Power and Neoclassical Economics_ A Return to Political Economy in the Teaching of Economics-Palgrave Macmillan UK (2016).pdf
│   │   ├── (Agora Series) Marc Lichtenfeld-Get Rich with Dividends_ A Proven System for Earning Double-Digit Returns-Wiley (2015).epub
│   │   ├── Algorithmic Trading and DMA.pdf
│   │   ├── Allen C. Benello, Michael van Biema, Tobias E. Carlisle Concentrated Investing Strategies of the World’s Greatest Concentrated Value Investors.pdf
│   │   ├── Alonso Peña-Advanced Quantitative Finance with C++-Packt Publishing (2014) (1).pdf
│   │   ├── Alonso Peña-Advanced Quantitative Finance with C++-Packt Publishing (2014).pdf
│   │   ├── Alonso Pena, Ph.D.-Advanced Quantitative Finance with C++_ Create and implement mathematical models in C++ using quantitative finance-Packt Publishing (2014).pdf
│   │   ├── Alpha101
│   │   ├── Andreas Binder, Michael Aichinger-A Workout in Computational Finance-Wiley (2013).pdf
│   │   ├── (Applied Quantitative Finance) Adil Reghai-Quantitative Finance_ Back to Basic Principles-Palgrave Macmillan (2015).pdf
│   │   ├── (Applied Quantitative Finance) Oliver Brockhaus-Equity Derivatives and Hybrids. Markets, Models and Methods-Palgrave Macmillan (2016).pdf
│   │   ├── (Applied Quantitative Finance series) Sergio Scandizzo (auth.)-The Validation of Risk Models_ A Handbook for Practitioners-Palgrave Macmillan UK (2016).pdf
│   │   ├── Applied Quantitative Methods for Trading and Investment.pdf
│   │   ├── Art Collins, Robert Pardo-Beating the Financial Futures Market_ Combining Small Biases into Powerful Money Making Strategies (Wiley Trading)-Wiley (2006).pdf
│   │   ├── (Atlantis Studies in Computational Finance and Financial Engineering 1) Argimiro Arratia (auth.)-Computational Finance_ An Introductory Course with R-Atlantis Press (2014).pdf
│   │   ├── Automated Trading with R.pdf
│   │   ├── (BestMasters) Markus Vollmer (auth.)-A Beta-return Efficient Portfolio Optimisation Following the CAPM_ An Analysis of International Markets and Sectors-Gabler Verlag (2015).pdf
│   │   ├── Bill Kraft-The Smart Investor's Money Machine_ Methods and Strategies to Create Regular Income (Wiley Trading) (2009).pdf
│   │   ├── (Chapman and Hall_CRC Financial Mathematics Series) Schlogl, Erik-Quantitative Finance _ An Object-Oriented Approach in C++-CRC Press (2013).pdf
│   │   ├── Charles M. Judd, Gary H. McClelland, Carey S. Ryan-Data Analysis_ A Model Comparison Approach To Regression, ANOVA, and Beyond-Routledge (2017).pdf
│   │   ├── Charlie Tian-Invest Like a Guru_ How to Generate Higher Returns At Reduced Risk With Value Investing-John Wiley & Sons Inc. (2017).epub
│   │   ├── Cliff Asness-Short Selling_ Strategies, Risks, and Rewards-Wiley (2004).pdf
│   │   ├── Commodities-and-Commodity-Derivatives-Modelling-and-Pricing-for-Agriculturals-Metals-and-Energy.pdf
│   │   ├── Consigli, Giorgio_ Stefani, Silvana_ Zambruno, Giovanni-Handbook of Recent Advances in Commodity and Financial Modeling_ Quantitative Methods in Banking, Finance, Insurance, Energy and Commodity Marke.pdf
│   │   ├── E. J. Stavetski-Managing Hedge Fund Managers_ Quantitative and Qualitative Performance Measures (Wiley Finance) (2009).pdf
│   │   ├── Empirical Market Microstructure.pdf
│   │   ├── Eugene F. Fama-Foundations of Finance_ Portfolio Decisions and Securities Prices 1976-06 (1976).pdf
│   │   ├── Financial Econometrics and Empirical Market Microstructure.pdf
│   │   ├── Financial Markets and Trading.pdf
│   │   ├── (Frank J. Fabozzi Series) Frank J. Fabozzi, Dessislava A. Pachamanova-Portfolio Construction and Analytics-Wiley (2016).pdf
│   │   ├── George Kleinman-Trading Commodities and Financial Futures - A Step-by-Step Guide to Mastering the Markets-FT Press (2013).epub
│   │   ├── (Global Financial Markets) Tom James (auth.)-Commodity Market Trading and Investment_ A Practitioners Guide to the Markets-Palgrave Macmillan UK (2016).pdf
│   │   ├── Greg N. Gregoriou, Razvan Pascalau-Financial Econometrics Modeling_ Market Microstructure, Factor Models and Financial Risk Measures-Palgrave Macmillan (2011).pdf
│   │   ├── Handbook of Recent Advances in Commodity and Financial Modeling_ Quantitative Methods in Banking, Finance, Insurance, Energy and Commodity Marke.pdf
│   │   ├── Harry Markowitz, Kenneth Blay-Risk-Return Analysis_ The Theory and Practice of Rational Investing (Volume One)-McGraw-Hill (2013).pdf
│   │   ├── High-Frequency Trading  A Practical Guide to Algorithmic Strategies and Trading Systems.pdf
│   │   ├── Howard B. Bandy-Quantitative Trading Systems_ Practical Methods for Design, Testing, and Validation-Blue Owl Press (2007).pdf
│   │   ├── (Interdisciplinary Mathematical Sciences 14) Thomas Gerstner, Thomas Gerstner, Peter Kloede-Recent Developments in Computational Finance_ Foundations, Algorithms and Applications-World Scientific Publ.pdf
│   │   ├── (International Series in Operations Research &amp_ Management Science 245) Giorgio Consigli, Daniel Kuhn, Paolo Brandimarte (eds.)-Optimal Financial Decision Making under Uncertainty-Springer Internat.pdf
│   │   ├── (International Series in Operations Research & Management Science 163) William T. Ziemba, Leonard C. MacLean (auth.), Marida Bertocchi, Giorgio Consigli, Michael A. H. Dempster (eds.)-Stochastic Optim.pdf
│   │   ├── James A. Primbs-A Factor Model Approach to Derivative Pricing-Chapman and Hall_CRC (2016).pdf
│   │   ├── James A. Primbs-A Factor Model Approach to Derivative Pricing-CRC (2014).pdf
│   │   ├── Jeremy J. Siegel-Stocks for the Long Run_  The Definitive Guide to Financial Market Returns & Long-Term Investment Strategies-McGraw-Hill Education (2014).pdf
│   │   ├── Joel Greenblatt-The Little Book That Still Beats the Market (Little Books. Big Profits)-Wiley (2010).pdf
│   │   ├── Kenneth H. Shaleen-Technical Analysis & Options Strategies-Probus Professional Pub (1992).pdf
│   │   ├── Learning Quantitative Finance with R.pdf
│   │   ├── Learning Quantitative Finance with R.zip
│   │   ├── (Lecture Notes in Economics and Mathematical Systems 545) Dr. Reinhold Hafner (auth.)-Stochastic Implied Volatility_ A Factor-Based Model-Springer Berlin Heidelberg (2004).pdf
│   │   ├── (Lecture Notes in Economics and Mathematical Systems 660) Marcus Brandenburg (auth.)-Quantitative Models for Value-Based Supply Chain Management-Springer-Verlag Berlin Heidelberg (2013).pdf
│   │   ├── (Little Books. Big Profits) John C. Bogle-The Little Book of Common Sense Investing_ The Only Way to Guarantee Your Fair Share of Stock Market Returns-Wiley (2017).epub
│   │   ├── Luke L. Wiley, Wesley R. Gray The 52-Week Low Formula A Contrarian Strategy that Lowers Risk, Beats the Market, and Overcomes Human Emotion.epub
│   │   ├── [Madhavan_A.]_Market_microstructure_A_practitioner_guide.pdf
│   │   ├── (Methodology in the Social Sciences) Darlington, Richard B._ Hayes, Andrew F._ Little, Todd D-Regression Analysis and Linear Models_ Concepts, Applications, and Implementation-Guilford Publications_Th.pdf
│   │   ├── Michael C. I. Nwogugu (auth.)-Anomalies in Net Present Value, Returns and Polynomials, and Regret Theory in Decision-Making -Palgrave Macmillan UK (2016).pdf
│   │   ├── Michael Halls Moore-Advanced Algorithmic Trading (2017).pdf
│   │   ├── Models-Behaving-Badly.epub
│   │   ├── Models-Behaving-Badly-Why-Confusing-Illusion-with-Reality-Can-Lead-to-Disaster-on-Wall-Street-and-in-Life-.epub
│   │   ├── Momentum Trading
│   │   ├── (New Developments in Quantitative Trading and Investment) Christian L. Dunis, Peter W. Middleton, Andreas Karathanasopolous, Konstantinos Theofilatos (eds.)-Artificial Intelligence in Financial Market (1).pdf
│   │   ├── (New Developments in Quantitative Trading and Investment) Christian L. Dunis, Peter W. Middleton, Andreas Karathanasopolous, Konstantinos Theofilatos (eds.)-Artificial Intelligence in Financial Market.pdf
│   │   ├── (Optimization in insurance and finance set) Gushchin, Alexander A-Mathematical basis for finance _ Stochastic calculus for quantitative finance-ISTE Press Ltd _ Kidlington (2015).pdf
│   │   ├── Pairs Trading Quantitative Methods and Analysis.pdf
│   │   ├── Paul Wilmott-Frequently asked questions in quantitative finance_ including key models, important formul, popular contracts, essays and opinions, a history of quantitative finance, sundry lists, the co.pdf
│   │   ├── Pawel Lachowicz-Python for Quants. 1-QuantAtRisk (2015).pdf
│   │   ├── Piotr Staszkiewicz, Lucia Staszkiewicz-Finance_ A Quantitative Introduction-Academic Press (2014).pdf
│   │   ├── Quantitative Analysis of Market Data.pdf
│   │   ├── (Quantitative Finance) Alan Scowcroft, Stephen Satchell-Advances in Portfolio Construction and Implementation-Butterworth-Heinemann (2003).pdf
│   │   ├── (Quantitative Finance) George Levy-Computational Finance Using C and C_. Derivatives and Valuation-Academic Press (2016) (1).pdf
│   │   ├── (Quantitative Finance) George Levy-Computational Finance Using C and C_. Derivatives and Valuation-Academic Press (2016).pdf
│   │   ├── (Quantitative Finance) John Knight, Stephen Satchell-Linear factor models in finance-Butterworth-Heinemann (2005).pdf
│   │   ├── (Quantitative finance series) Stephen Satchell-Forecasting Expected Returns in the Financial Markets-Academic Press (2007).pdf
│   │   ├── Quantitative Investment Analysis.pdf
│   │   ├── Quantitative Investment Analysis, Workbook.pdf
│   │   ├── Quantitative Methods for Investment Analysis.pdf
│   │   ├── (Quantitative Perspectives on Behavioral Economics and Finance) James Ming Chen (auth.)-Finance and the Behavioral Prospect_ Risk, Exuberance, and Abnormal Markets-Palgrave Macmillan (2016).pdf
│   │   ├── (Quantitative Perspectives on Behavioral Economics and Finance) James Ming Chen (auth.)-Postmodern Portfolio Theory_  Navigating Abnormal Markets and Investor Behavior-Palgrave Macmillan US (2016) (1).pdf
│   │   ├── (Quantitative Perspectives on Behavioral Economics and Finance) James Ming Chen (auth.)-Postmodern Portfolio Theory_  Navigating Abnormal Markets and Investor Behavior-Palgrave Macmillan US (2016).pdf
│   │   ├── Robert Kissell and Jim Poserina (Auth.)-Optimal Sports Math, Statistics, and Fantasy- Academic Press  (2017).pdf
│   │   ├── Robert Kissell (Auth.)-The Science of Algorithmic Trading and Portfolio Management-Academic Press (2013) (1).pdf
│   │   ├── Robert Kissell (Auth.)-The Science of Algorithmic Trading and Portfolio Management-Academic Press (2013).pdf
│   │   ├── Robert R. Reitano-Introduction to Quantitative Finance_ A Math Tool Kit-The MIT Press (2010).pdf
│   │   ├── Ronald Christensen-Analysis of Variance Design and Regression Linear Modeling for Unbalanced Data-CRC (2017).pdf
│   │   ├── (Routledge Advances in Experimental and Computable Economics) Christian Dunis, Spiros Likothanassis, Andreas Karathanasopoulos, Georgios Sermpinis, Konstantinos Theofilatos-Computational Intelligence .pdf
│   │   ├── Serges Darolles, Patrick Duvaut, Emmanuelle Jay-Multi-factor Models and Signal Processing Techniques_ Application to Quantitative Finance-Wiley-ISTE (2013).pdf
│   │   ├── (SpringerBriefs in Mathematics) Anatoliy Swishchuk (auth.)-Change of Time Methods in Quantitative Finance-Springer International Publishing (2016).pdf
│   │   ├── (SpringerBriefs in Quantitative Finance) Maria Elvira Mancino, Maria Cristina Recchioni, Simona Sanfelici (auth.)-Fourier-Malliavin Volatility Estimation_ Theory and Practice-Springer International Pu.pdf
│   │   ├── (SpringerBriefs in Quantitative Finance) Tim Leung, Marco Santoli (auth.)-Leveraged Exchange-Traded Funds_ Price Dynamics and Options Valuation-Springer International Publishing (2016).pdf
│   │   ├── (Springer Finance) Gianluca Fusai, Andrea Roncoroni-Implementing models in quantitative finance_ methods and cases-Springer (2008).pdf
│   │   ├── (Springer Finance) Norbert Hilber, Oleg Reichmann, Christoph Schwab, Christoph Winter-Computational Methods for Quantitative Finance_ Finite Element Methods for Derivative Pricing-Springer (2013).pdf
│   │   ├── (Springer Handbooks of Computational Statistics) Jin-Chuan Duan, James E. Gentle, Wolfgang Karl Härdle (auth.), Jin-Chuan Duan, Wolfgang Karl Härdle, James E. Gentle (eds.)-Handbook of Computational F.pdf
│   │   ├── (Springer Texts in Business and Economics) Wolfgang Marty (auth.)-Portfolio Analytics_ An Introduction to Return and Risk Measurement-Springer International Publishing (2015).pdf
│   │   ├── (Statistics and computing) Chen, Cathy Yi-Hsuan_ Härdle, Wolfgang_ Overbeck, Ludger-Applied quantitative finance-Springer (2017) (1).pdf
│   │   ├── (Statistics and computing) Chen, Cathy Yi-Hsuan_ Härdle, Wolfgang_ Overbeck, Ludger-Applied quantitative finance-Springer (2017) (2).pdf
│   │   ├── (Statistics and computing) Chen, Cathy Yi-Hsuan_ Härdle, Wolfgang_ Overbeck, Ludger-Applied quantitative finance-Springer (2017) (3).pdf
│   │   ├── (Statistics and computing) Chen, Cathy Yi-Hsuan_ Härdle, Wolfgang_ Overbeck, Ludger-Applied quantitative finance-Springer (2017).pdf
│   │   ├── Statistics and Data Analysis for Financial Engineering
│   │   ├── Statistics and Data Analysis for Financial Engineering.pdf
│   │   ├── Statistics and Finance An Introduction.pdf
│   │   ├── Stephen Satchell (eds.)-Asset Management_ Portfolio Construction, Performance and Returns-Palgrave Macmillan (2016).pdf
│   │   ├── Steve Bell-Quantitative Finance For Dummies-For Dummies (2016).pdf
│   │   ├── SystematicTrading
│   │   ├── Tadas Viskanta-Abnormal Returns_ Winning Strategies from the Frontlines of the Investment Blogosphere-McGraw-Hill (2012).epub
│   │   ├── The art and science of technical analysis.pdf
│   │   ├── The-microstructure-approach-to-exchange-rates.pdf
│   │   ├── (The Wiley Finance Series) Christian L. Dunis, Jason Laws, Patrick Naïm-Applied quantitative methods for trading and investment-Wiley (2009).pdf
│   │   ├── (The Wiley Finance Series) Paul Darbyshire, David Hampton-Hedge Fund Modelling and Analysis  An Object Oriented Approach Using C++-Wiley (2016).pdf
│   │   ├── Tobias E. Carlisle Deep Value Why Activist Investors and Other Contrarians Battle for Control of Losing Corporations.pdf
│   │   ├── Trading and Exchanges Market Microstructure for Practitioners .pdf
│   │   ├── Trading-Volatility-Trading-Volatility-Correlation-Term-Structure-and-Skew.pdf
│   │   ├── Van K Tharp-Trade Your Way to Financial Freedom-McGraw-Hill Companies (2006).pdf
│   │   ├── Van Tharp-Super Trader. Make Consistent Profits in Good and Bad Markets-McGraw-Hill (2009).pdf
│   │   ├── Van Tharp-Trading Beyond the Matrix_ The Red Pill for Traders and Investors-Wiley (2013).epub
│   │   ├── Volatility-Trading-Second-Edition.pdf
│   │   ├── (Wiley finance series 702) Stephen D. Hassett-The Risk Premium Factor, + Website_ A New Model for Understanding the Volatile Forces that Drive Stock Prices-John Wiley & Sons (2011).pdf
│   │   ├── (Wiley Finance) Wesley Gray, Tobias Carlisle-Quantitative Value_ A Practitioner's Guide to Automating Intelligent Investment and Eliminating Behavioral Errors-Wiley (2012).epub
│   │   ├── (Wiley Finance) Wesley R. Gray, Jack R. Vogel-Quantitative Momentum_ A Practitioner’s Guide to Building a Momentum-Based Stock Selection System-Wiley (2016).pdf
│   │   ├── (Wiley Series in Computational Statistics) Jussi Klemelä-Multivariate Nonparametric Regression and Visualization_ With R and Applications to Finance-Wiley-Interscience (2014).pdf
│   │   ├── (Wiley Series in Probability and Statistics) David J. Bartholomew, Martin Knott, Irini Moustaki-Latent Variable Models and Factor Analysis_ A Unified Approach-Wiley (2011).pdf
│   │   ├── (Wiley Trading) Bill Kraft-Trade Your Way to Wealth_ Earn Big Profits with No-Risk, Low-Risk, and Measured-Risk Strategies-Wiley (2008).pdf
│   │   ├── (Wiley Trading) Ernest P. Chan-Machine Trading_ Deploying Computer Algorithms to Conquer the Markets-Wiley (2017)_backup.pdf
│   │   ├── (Wiley Trading) Ernest P. Chan-Machine Trading_ Deploying Computer Algorithms to Conquer the Markets-Wiley (2017).pdf
│   │   ├── (Wiley Trading) Ernie Chan-Algorithmic Trading_ Winning Strategies and Their Rationale-Wiley (2013).pdf
│   │   └── (Wiley Trading) Michael C. Khouw, Mark W. Guthner-The Options Edge_ An Intuitive Approach to Generating Consistent Profits for the Novice to the Experienced Practitioner-Wiley (2016).pdf
│   ├── R
│   │   ├── 993C9914-4C50-4402-BAFB-36D1B20F9554 (1).pdf
│   │   ├── 993C9914-4C50-4402-BAFB-36D1B20F9554.pdf
│   │   ├── Beginning-Data-Science-in-R-Data-Analysis-Visualization-and-Modelling-for-the-Data-Scientist.pdf
│   │   ├── [Chapman & Hall_CRC The R Series] Deborah Nolan, Duncan Temple Lang - Data Science in R_ A Case Studies Approach to Computational Reasoning and Problem Solving (2015, Chapman and Hall_CRC).pdf
│   │   ├── Chris Beeley-Web Application Development with R Using Shiny-Packt Publishing (2013).pdf
│   │   ├── Christian Heumann, Michael Schomaker Shalabh - Introduction to Statistics and Data Analysis with Exercises, Solutions and Applications in R (2017, Springer).pdf
│   │   ├── Data Analysis with R.pdf
│   │   ├── efficient-master.zip
│   │   ├── Efficient_R_Programming_A_Practical_Guide_to_Smarter_Programming.pdf
│   │   ├── Efficient R Programming.pdf
│   │   ├── Extending-R.pdf
│   │   ├── [Hadley_Wickham_(auth.)]_ggplot2_Elegant_Graphics(b-ok.org).pdf
│   │   ├── Hadley Wickham, Garrett Grolemund-R for Data Science_ Import, Tidy, Transform, Visualize, and Model Data-O’Reilly Media (2017).pdf
│   │   ├── Hongshik Ahn - Probability and Statistics for Science and Engineering with Examples in R (2018, Cognella).pdf
│   │   ├── Julian J. Faraway-Extending the Linear Model with R. Generalized Linear, Mixed Effects and Nonparametric Regression Models-CRC (2017).pdf
│   │   ├── Laura M. Chihara, Tim C. Hesterberg - Mathematical Statistics with Resampling and R (2018, Wiley).pdf
│   │   ├── Learning_R_Programming_－_Become_an_efficient_data_scientist_with_R.pdf
│   │   ├── Mastering-Predictive-Analytics-with-R.pdf
│   │   ├── Max Kuhn, Kjell Johnson-Applied Predictive Modeling-Springer (2013).pdf
│   │   ├── [Monographs on statistics and applied probability (Series) 159.] Tian, Xin_ Wu, Colin O. - Nonparametric Models for Longitudinal Data _ With Implementation in R (2018, CRC Press).pdf
│   │   ├── N.D Lewis-Deep Learning Made Easy with R_ A Gentle Introduction For Data Science-CreateSpace Independent Publishing Platform (2016).pdf
│   │   ├── Raja B. Koushik, Sharan Kumar Ravindran-R Data Science Essentials-Packt (2016).pdf
│   │   ├── R-Data-Structures-and-Algorithms.pdf
│   │   ├── R Data Structures and Algorithms.zip
│   │   ├── R_Projects_For_Dummies.pdf
│   │   ├── Shuichi Ohsaki, Jori Ruppert-Felsot, Daisuke Yoshikawa - R Programming and Its Applications in Financial Mathematics (2018, CRC Press).pdf
│   │   ├── Simon Munzert, Christian Rubba, Peter Meißner, Dominic Nyhuis-Automated Data Collection with R_ A Practical Guide to Web Scraping and Text Mining-Wiley (2015).pdf
│   │   ├── [Springer Texts in Statistics] Robert H. Shumway, David S. Stoffer (auth.) - Time Series Analysis and Its Applications_ With R Examples (2017, Springer International Publishing).pdf
│   │   ├── Thomas Mailund (auth.)-Advanced Object-Oriented Programming in R_ Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
│   │   ├── Thomas Mailund (auth.)-Metaprogramming in R_ Advanced Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
│   │   ├── Thomas Mailund-Beginning Data Science in R_ Data Analysis, Visualization, and Modelling for the Data Scientist-Apress (2017).pdf
│   │   ├── Thomas Mailund-Functional Programming in R. Advanced Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
│   │   ├── Tony Fischetti-Data Analysis with R_ Load, wrangle, and analyze your data using the world's most powerful statistical programming language-Packt Publishing (2015).pdf
│   │   ├── (Use R! 65) Eric D. Kolaczyk, Gábor Csárdi (auth.)-Statistical Analysis of Network Data with R-Springer-Verlag New York (2014).pdf
│   │   └── [Use R!] Deborah Nolan, Duncan Temple Lang (auth.) - XML and Web Technologies for Data Sciences with R (2014, Springer-Verlag New York).pdf
│   ├── Social
│   │   └── 万历十五年.mobi
│   ├── Social_Science
│   │   ├── Alan Grafen, Mark Ridley-Richard Dawkins - How a Scientist Changed the Way We Think-Oxford University Press, USA (2007).pdf
│   │   ├── Jerry A. Coyne-Why Evolution Is True-Oxford University Press (2009).pdf
│   │   ├── Richard Dawkins-The Extended Phenotype_ The Long Reach of the Gene-Oxford University Press (1999).mobi
│   │   ├── Richard Dawkins-The Greatest Show on Earth_ The Evidence for Evolution  -Free Press (2009).mobi
│   │   └── Richard Dawkins-上帝的迷思-Hainan Publishing House (2006).pdf
│   ├── Stat
│   │   ├── (Academic Press Advanced Finance) Laurent E. Calvet, Adlai J. Fisher-Multifractal Volatility..Theory, Forecasting, and Pricing-Academic Press (2008).pdf
│   │   ├── Advanced Linear Modeling.pdf
│   │   ├── (Advances in Intelligent and Soft Computing 77) Danilo Abbate, Roberta De Asmundis (auth.), Christian Borgelt, Gil González-Rodríguez, Wolfgang Trutschnig, María Asunción Lubiano, María Ángeles Gil, P.pdf
│   │   ├── All Figures.zip
│   │   ├── All Labs.txt
│   │   ├── Andrea Ceron, Luigi Curini, Stefano Maria Iacus-Politics and Big Data_ Nowcasting and Forecasting Elections with Social Media-Routledge (2017).pdf
│   │   ├── Andrew Gelman, Jennifer Hill-Data Analysis Using Regression and Multilevel_Hierarchical Models-Cambridge University Press (2007).pdf
│   │   ├── Andrew Gelman, Jeronimo Cortina-A Quantitative Tour of the Social Sciences (2009).pdf
│   │   ├── An Elementary Introduction to Statistical Learning Theory.pdf
│   │   ├── An Introduction to Statistical Learning.pdf
│   │   ├── An Introduction to Statistical Learning with Applications in R.pdf
│   │   ├── Applying Regression and Correlation.pdf
│   │   ├── Ben Klemens - Modeling with data_ tools and techniques for scientific computing (2009, Princeton University Press).pdf
│   │   ├── (Bloomberg) Niklas Hageback-The Mystery of Market Movements_ An Archetypal Approach to Investment Forecasting and Modelling-Wiley (2014).epub
│   │   ├── (Book Only) Francis X. Diebold-Elements of Forecasting-Cengage Learning (2006).pdf
│   │   ├── Causal-Inference-in-Statistics-A-Primer.pdf
│   │   ├── (Chapman & Hall_CRC Texts in Statistical Science) Norman Matloff-Statistical Regression and Classification_ From Linear Models to Machine Learning-Chapman and Hall_CRC (2017).pdf
│   │   ├── [Chapman & Hall_CRC Texts in Statistical Science] Richard McElreath - Statistical Rethinking_ A Bayesian Course with Examples in R and Stan (2015, Chapman and Hall_CRC).pdf
│   │   ├── (Chapman & Hall_CRC the R series (CRC Press)) Eubank, Randall L._ Kupresanin, Ana-Statistical computing in C++ and R-CRC Press (2011).pdf
│   │   ├── (Computer Science and Scientific Computing Series) Keinosuke Fukunaga-Introduction to Statistical Pattern Recognition-Academic Press (1990).pdf
│   │   ├── (Contributions to statistics) Pomares, Héctor_ Rojas, Ignacio_ Valenzuela, Olga-Advances in time series analysis and forecasting _ selected contributions from ITISE 2016-Springer (2017).pdf
│   │   ├── David J. Olive-Linear Regression-Springer (2017).pdf
│   │   ├── David M Diez, Christopher D Barr, Mine Çetinkaya-Rundel - OpenIntro Statistics (2015, OpenIntro, Inc.).pdf
│   │   ├── Deborah Nolan, Terry P. Speed - Stat Labs Mathematical Statistics Through Applications (2001, Springer).pdf
│   │   ├── (Economics collection) Vu, Tam Bang-Seeing the future _ how to build basic forecasting models-Business Expert Press (2015).pdf
│   │   ├── Elements of Statistics for the Life and Social Sciences.pdf
│   │   ├── (Financial accounting and auditing collection) Bettner, Mark S-Using accounting & financial information _ analyzing, forecasting & decision making-Business Expert Press (2015).pdf
│   │   ├── Foundations of Statistical Algorithms.pdf
│   │   ├── Franses P.H., Dijk D.v., Opschoor A.-Time Series Models for Business and Economic Forecasting-CUP (2014).pdf
│   │   ├── (Frontiers in Probability and the Statistical Sciences) Dimitris N. Politis (auth.)-Model-Free Prediction and Regression_ A Transformation-Based Approach to Inference-Springer International Publishing.pdf
│   │   ├── (Graduate Texts in Physics) Massimiliano Bonamente (auth.)-Statistics and Analysis of Scientific Data-Springer-Verlag New York (2017).pdf
│   │   ├── Graham Elliott, Allan Timmermann-Economic Forecasting-Princeton University Press (2016).pdf
│   │   ├── Holger Kömm (auth.)-Forecasting High-Frequency Volatility Shocks_ An Analytical Real-Time Monitoring System-Gabler Verlag (2016).pdf
│   │   ├── Introduction-to-Linear-Regression-Analysis.pdf
│   │   ├── Introduction to Statistical Machine Learning.pdf
│   │   ├── Jan G. De Gooijer-Elements of Nonlinear Time Series Analysis and Forecasting-Springer (2017).pdf
│   │   ├── John B. Guerard, Jr. (auth.)-Introduction to Financial Forecasting in Investment Analysis-Springer-Verlag New York (2013).pdf
│   │   ├── John Pirc, David DeSanto, Iain Davison, Will Gragido-Threat Forecasting. Leveraging Big Data for Predictive Analysis-Syngress (2016).pdf
│   │   ├── Joseph M. Hilbe-Practical guide to logistic regression-Taylor & Francis (2016).pdf
│   │   ├── Judea Pearl - Causality_ Models, Reasoning and Inference (2009, Cambridge University Press).pdf
│   │   ├── Judea Pearl, Madelyn Glymour, Nicholas P. Jewell - Causal Inference in Statistics_ A Primer (2016, Wiley).pdf.crdownload
│   │   ├── Julian J. Faraway-Extending the Linear Model with R. Generalized Linear, Mixed Effects and Nonparametric Regression Models-CRC (2017).pdf
│   │   ├── Koning, Jan de_ Vliet, Pim van-High returns from low risk_ a remarkable stock market paradox-Wiley (2017).pdf
│   │   ├── Large-Scale-Inference-Empirical-Bayes-Methods-for-Estimation-Testing-and-Prediction.pdf
│   │   ├── (Lecture Notes in Control and Information Sciences 442) Xiaolian Zheng, Ben M. Chen (auth.)-Stock Market Modeling and Forecasting_ A System Adaptation Approach-Springer-Verlag London (2013).pdf
│   │   ├── (Lecture Notes in Economics and Mathematical Systems 623) Christian Ullrich (auth.)-Forecasting and Hedging in the Foreign Exchange Markets-Springer-Verlag Berlin Heidelberg (2009).pdf
│   │   ├── (Lecture Notes in Statistics 217) Anestis Antoniadis, Jean-Michel Poggi, Xavier Brossat (eds.)-Modeling and Stochastic Learning for Forecasting in High Dimensions-Springer International Publishing (20.pdf
│   │   ├── Maria L. Rizzo-Statistical computing with R. 1-Chapman & Hall _ CRC (2007).pdf
│   │   ├── Massaron, Luca & Boschetti, Alberto [Massaron, Luca]-Regression Analysis with Python-Packt Publishing (2016).pdf
│   │   ├── (Methodology in the Social Sciences) Darlington, Richard B._ Hayes, Andrew F._ Little, Todd D-Regression Analysis and Linear Models_ Concepts, Applications, and Implementation-Guilford Publications_Th.pdf
│   │   ├── (Methodology in the Social Sciences) Richard B. Darlington PhD, Andrew F. Hayes PhD-Regression Analysis and Linear Models_ Concepts, Applications, and Implementation-The Guilford Press (2016).pdf
│   │   ├── N D Lewis-Deep Time Series Forecasting with Python_ An Intuitive Introduction to Deep Learning for Applied Time Series Modeling-CreateSpace Independent Publishing Platform (2016).pdf
│   │   ├── N.D. Lewis-Neural Networks for Time Series Forecasting with R-N.D. Lewis (2017).pdf
│   │   ├── Pearl, Judea_ Mackenzie, Dana - The Book of Why_ The New Science of Cause and Effect (2018, Basic Books).epub (1).crdownload
│   │   ├── Pearl, Judea_ Mackenzie, Dana - The Book of Why_ The New Science of Cause and Effect (2018, Basic Books).epub (2).crdownload
│   │   ├── Pearl, Judea_ Mackenzie, Dana - The Book of Why_ The New Science of Cause and Effect (2018, Basic Books).epub (3).crdownload
│   │   ├── Pearl, Judea_ Mackenzie, Dana - The Book of Why_ The New Science of Cause and Effect (2018, Basic Books).epub.crdownload
│   │   ├── Philip Campbell, Steve Player-A Quick Start Guide to Financial Forecasting_ Discover the Secret to Driving Growth, Profitability, and Cash Flow Higher-Grow & Succeed Publishing Llc (2017).epub
│   │   ├── Plane Answers to Complex Questions The Theory of Linear Models.pdf
│   │   ├── Rachel A. Gordon-Regression Analysis for the Social Sciences-Routledge (2015).pdf
│   │   ├── Raghu Raj Bahadur, Stephen M. Stigler, Wing Hung Wong, Daming Xu - R.R. Bahadur's lectures on the theory of estimation (2002, IMS).pdf
│   │   ├── Roberto S. Mariano-Econometric Forecasting And High-Frequency Data Analysis (Lecture Notes Seres, Institute for Mathematical Sciences National University of Singapore) (2008).pdf
│   │   ├── Solutions-Manual-to-Accompany-Introduction-to-Linear-Regression-Analysis.pdf
│   │   ├── (SpringerBriefs in Statistics) Puntanen Simo, Styan George P. H., Isotalo Jarkko (auth.)-Formulas Useful for Linear Regression Analysis and Related Matrix Theory_ It's Only Formulas But We Like Them-S.pdf
│   │   ├── (Springer Series in Statistics) Frank E. Harrell , Jr. (auth.)-Regression Modeling Strategies_ With Applications to Linear Models, Logistic and Ordinal Regression, and Survival Analysis-Springer Inter.pdf
│   │   ├── (Springer series in statistics) Rob J. Hyndman ... [et al.]-Forecasting with exponential smoothing _ the state space approach-Springer (2008).pdf
│   │   ├── (Springer Texts in Statistics) Anirban DasGupta (auth.)-Probability for Statistics and Machine Learning_ Fundamentals and Advanced Topics-Springer-Verlag New York (2011).pdf
│   │   ├── (Springer Texts in Statistics) Dennis D. Boos, L A Stefanski-Essential Statistical Inference_ Theory and Methods-Springer (2013).pdf
│   │   ├── (Springer Texts in Statistics) James E. Gentle (auth.)- Matrix Algebra_ Theory, Computations and Applications in Statistics-Springer International Publishing (2017).pdf
│   │   ├── (Springer Texts in Statistics) Matthew A. Carlton, Jay L. Devore (auth.)-Probability with Applications in Engineering, Science, and Technology-Springer International Publishing (2017).pdf
│   │   ├── (Springer Texts in Statistics) Peter J. Brockwell, Richard A. Davis (auth.)-Introduction to Time Series and Forecasting-Springer International Publishing (2016).pdf
│   │   ├── (Springer Texts in Statistics) Rabi Bhattacharya, Lizhen Lin, Victor Patrangenaru (auth.)-A Course in Mathematical Statistics and Large Sample Theory-Springer-Verlag New York (2016).pdf
│   │   ├── (Springer Texts in Statistics) Richard A. Berk (auth.)-Statistical Learning from a Regression Perspective-Springer International Publishing (2016).pdf
│   │   ├── (Springer Texts in Statistics) Richard Durrett-Essentials of Stochastic Processes-Springer (2016).pdf
│   │   ├── (Springer Texts in Statistics) Robert H. Shumway, David S. Stoffer (auth.)-Time Series Analysis and Its Applications_ With R Examples-Springer International Publishing (2017) (1).pdf
│   │   ├── (Springer Texts in Statistics) Robert H. Shumway, David S. Stoffer (auth.)-Time Series Analysis and Its Applications_ With R Examples-Springer International Publishing (2017).pdf
│   │   ├── (Springer texts in statistics) Shorack, Galen R-Probability for statisticians-Springer (2017).pdf
│   │   ├── (Springer Texts in Statistics) Vladimir Spokoiny, Thorsten Dickhaus (auth.)-Basics of Modern Mathematical Statistics-Springer-Verlag Berlin Heidelberg (2015).pdf
│   │   ├── Statistical Analysis and Data Display.pdf
│   │   ├── Statistical_Learning
│   │   ├── (Statistics and Computing) Graham Wills (auth.)-Visualizing Time_ Designing Graphical Representations for Statistical Data  -Springer-Verlag New York (2012).pdf
│   │   ├── (Statistics for Biology and Health) George J. Knafl, Kai Ding (auth.)-Adaptive Regression for Modeling Nonlinear Relationships-Springer International Publishing (2016).pdf
│   │   ├── Statistics_for_Finance.pdf
│   │   ├── Stavros Degiannakis, Christos Floros (auth.)-Modelling and Forecasting High Frequency Financial Data-Palgrave Macmillan UK (2015).pdf
│   │   ├── Stephen M. Stigler - Statistics on the Table (1999, Harvard University Press).djvu
│   │   ├── Stephen M. Stigler - The history of statistics_ the measurement of uncertainty before 1900 (1986, Belknap Press).djvu
│   │   ├── Stephen M. Stigler - The Seven Pillars of Statistical Wisdom (2016, Harvard University Press).epub
│   │   ├── (Studies in Fuzziness and Soft Computing 330) Pritpal Singh (auth.)-Applications of Soft Computing in Time Series Forecasting_ Simulation and Modeling Techniques-Springer International Publishing (201 (1).pdf
│   │   ├── (Studies in Fuzziness and Soft Computing 330) Pritpal Singh (auth.)-Applications of Soft Computing in Time Series Forecasting_ Simulation and Modeling Techniques-Springer International Publishing (201.pdf
│   │   ├── (The Wiley Finance Series) Michael Samonas-Financial Forecasting, Analysis and Modelling_ A Framework for Long-Term Forecasting-Wiley (2015).pdf
│   │   ├── (The Wiley Finance Series) Timothy Jury-Cash Flow Analysis and Forecasting_ The Definitive Guide to Understanding and Using Published Cash Flow Data-Wiley (2012).pdf
│   │   ├── Timothy Z. Keith-Multiple Regression and Beyond_ An Introduction to Multiple Regression and Structural Equation Modeling-Routledge (2015).pdf
│   │   ├── (Volkswirtschaftliche Analysen, Bd. 19) Tina Loll-Forecasting economic time series using locally stationary processes _ a new approach with applications-Peter Lang (2012).pdf
│   │   ├── (Wiley Series in Computational Statistics) Faming Liang, Chuanhai Liu, Raymond Carroll-Advanced Markov chain Monte Carlo methods-Wiley (2010).pdf
│   │   ├── (Wiley Series in Computational Statistics) Jochen Voss-An Introduction to Statistical Computing_ A Simulation-based Approach-Wiley (2013).pdf
│   │   ├── (Wiley Series in Computational Statistics) Jussi Klemelä-Multivariate Nonparametric Regression and Visualization_ With R and Applications to Finance-Wiley-Interscience (2014).pdf
│   │   ├── (Wiley Series in Computational Statistics) Stéphane Tufféry-Data Mining and Statistics for Decision Making-Wiley (2011).pdf
│   │   ├── (Wiley Series in Probability and Statistics) Douglas C. Montgomery, Cheryl L. Jennings, Murat Kulahci-Introduction to Time Series Analysis and Forecasting-Wiley (2015).pdf
│   │   ├── (Wiley Series in Probability and Statistics) Etienne de Rocquigny(auth.), Walter A. Shewhart, Samuel S. Wilks(eds.)-Modelling Under Risk and Uncertainty_ An Introduction to Statistical, Phenomenologic (1).pdf
│   │   ├── (Wiley Series in Probability and Statistics) Etienne de Rocquigny(auth.), Walter A. Shewhart, Samuel S. Wilks(eds.)-Modelling Under Risk and Uncertainty_ An Introduction to Statistical, Phenomenologic.pdf
│   │   └── (Wiley Series in Probability and Statistics) George E. P. Box, Gwilym M. Jenkins, Gregory C. Reinsel, Greta M. Ljung-Time Series Analysis_ Forecasting and Control-Wiley (2015).pdf
│   ├── Tech
│   │   ├── (Advances in Intelligent Systems and Computing 456) Maria Brigida Ferraro, Paolo Giordani, Barbara Vantaggi, Marek Gagolewski, María Ángeles Gil, Przemysław Grzegorzewski, Olgierd Hryniewicz (eds.)-So.pdf
│   │   ├── Max Tegmark-Life 3.0_ Being Human in the Age of Artificial Intelligence-Alfred A. Knopf (2017).epub
│   │   └── Max Tegmark-Our Mathematical Universe_ My Quest for the Ultimate Nature of Reality-Knopf (2014).epub
│   ├── Trading
│   │   ├── Emilio Tomasini, Urban Jaekle-Trading Systems_ A New Approach to System Optimisation and Portfolio Construction-Harriman House (2009).pdf
│   │   ├── Gary Dayton-Trade Mindfully_ Achieve Your Optimum Trading Performance with Mindfulness and _Cutting Edge_ Psychology-Wiley (2014).pdf
│   │   ├── Howard B. Bandy-Quantitative Trading Systems_ Practical Methods for Design, Testing, and Validation-Blue Owl Press (2007).pdf
│   │   ├── Keith Fitschen(auth.)-Building Reliable Trading Systems_ Tradable Strategies That Perform as They Backtest and Meet Your Risk-Reward Goals-Wiley (2013).pdf
│   │   ├── Maybury, Matthew-Day Trading_ A Beginner's Guide To Day Trading_ Learn The Day Trading Basics To Building Riches (2016).epub
│   │   ├── (Quantitative Finance) Emmanual Acar, Stephen Satchell-Advanced Trading Rules-Butterworth-Heinemann (2002).pdf
│   │   ├── Robert Kissell, Morton Glantz-Optimal trading strategies_ quantitative approaches for managing market impact and trading risk-AMACOM (2003).djvu
│   │   ├── (Routledge Advances in Experimental and Computable Economics) Christian Dunis, Spiros Likothanassis, Andreas Karathanasopoulos, Georgios Sermpinis, Konstantinos Theofilatos-Computational Intelligence .pdf
│   │   ├── (Wiley Trading, 80) Greg Capra-Intra-Day Trading Tactics_ Pristine.com’s Stategies for Seizing Short-Term Opportunities-Wiley (2007).pdf
│   │   ├── (Wiley Trading) Harry Boxer-Profitable Day and Swing Trading_ Using Price_Volume Surges and Pattern Recognition to Catch Big Moves in the Stock Market-Wiley (2014).mobi
│   │   ├── (Wiley Trading) Kathy Lien-Day Trading and Swing Trading the Currency Market_ Technical and Fundamental Strategies to Profit from Market Moves-Wiley (2015).pdf
│   │   ├── (Wiley Trading) Michael Harris-Profitability and Systematic Trading_ A Quantitative Approach to Profitability, Risk, and Money Management -Wiley (2008).pdf
│   │   ├── (Wiley Trading) Perry J. Kaufman-Trading Systems and Methods, + Website-Wiley (2013).pdf
│   │   ├── (Wiley Trading) Robert Pardo-The Evaluation and Optimization of Trading Strategies-Wiley (2008).pdf
│   │   ├── (Wiley trading series) Kathy Lien-Day trading the currency market _ technical and fundamental strategies to profit from market swings-John Wiley & Sons  (2006).pdf
│   │   ├── (Wiley Trading Series) Kevin J. Davey-Building Winning Algorithmic Trading Systems_ A Trader's Journey from Data Mining to Monte Carlo Simulation to Live Trading-Wiley (2014).pdf
│   │   ├── (Wiley Trading) Thomas L. Busby, Patsy Busby Dow-Winning the Day Trading Game_ Lessons and Techniques from a Lifetime of Trading-Wiley (2005).pdf
│   │   ├── (Wiley Trading) Thomas L. Busby, Patsy Busby Dow-Winning the Day Trading Game_ Lessons and Techniques from a Lifetime of Trading-Wiley (2005).pdf.crdownload
│   │   ├── (Wiley trading) Thomas N. Bulkowski-Swing and day trading_ evolution of a trader-Wiley (2013).pdf
│   │   ├── Xin Guo, Tze Leung Lai, Howard Shek, Samuel Po-Shing Wong-Quantitative Trading_ Algorithms, Analytics, Data, Models, Optimization-Chapman and Hall_CRC (2016).azw3
│   │   └── 知乎盐系列+57+我是高频交易工程师+.mobi
│   └── Unix
│       ├── Anoop Chaturvedi, B.L. Rai-Unix and Shell Programming-Laxmi Publications (2017).pdf
│       ├── B. Kernighan, R. Pike UNIX - Programming Environment .pdf
│       ├── Chris Johnson, Jayant Varma-Pro Bash Programming, Second Edition_ Scripting the GNU_Linux Shell-Apress (2015).pdf
│       ├── Computer-Architecture-Sixth-Edition-A-Quantitative-Approach.pdf
│       ├── Dave Taylor and  Brandon Perry-Wicked Cool Shell Scripts_ 101 Scripts for Linux, OS X, and UNIX Systems-No Starch (2017).pdf
│       ├── Dave Taylor, Brandon Perry-Wicked Cool Shell Scripts_ 101 Scripts for Linux, OS X, and UNIX Systems-No Starch Press (2016).djvu
│       ├── (Developer’s Library) Stephen G. Kochan, Patrick Wood-Shell Programming in Unix, Linux and OS X-Addison-Wesley Professional (2016).pdf
│       ├── (developer’s series) William “Bo” Rothwell-Linux for Developers Jumpstart Your Linux Programming Skills-Addison-Wesley (2017).pdf
│       ├── Mark G. Sobell, Matthew Helmke - A practical Guide to Linux Commands, Editors and Shell Programming (2018, Addison-Wesley).pdf
│       ├── Mihalis Tsoukalos-Go Systems Programming_ Master Linux and Unix system level programming with Go-Packt Publishing (2017).pdf
│       ├── Mitja Resman-CentOS High Availability_ Leverage the power of high availability clusters on CentOS Linux, the enterprise-class, open source operating system-Packt Publishing (2015).pdf
│       ├── Robert Love-Linux system programming_ talking directly to the kernel and C library-O'Reilly Media (2013).pdf
│       ├── (The expert's voice in Linux) Nathan Campi, Kirk Bauer-Automating Linux and Unix System Administration-Apress_ Distributed to the book trade by Springer-Verlag (2009).pdf
│       ├── Timothy Boronczyk-CentOS 7 Server Deployment Cookbook-Packt Publishing (2016).azw3
│       └── W. Richard Stevens, Stephen A. Rago Advanced Programming in the UNIX Environment.pdf
├── README.md
└── Ref
    ├── momentum.bib
    └── momentum.bib.sav

29 directories, 644 files
```
